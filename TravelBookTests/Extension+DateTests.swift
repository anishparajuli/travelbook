//
//  TravelBookTests.swift
//  TravelBookTests
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import XCTest
@testable import TravelBook

class DateExtensionTests: XCTestCase {
    var sut: String!

    func test_Variable_AsDate_ReturnsCorrectYear_ForValidDateString() {
        sut = "2018-07-12T15:13:33.931Z"
        guard let date = Formatter.iso8601.date(from: sut) else  {
            XCTFail("Should not return nil with correct date format")
            return
        }
        XCTAssert(date.year == "2018")
    }
    
    func test_Variable_AsDate_ReturnsNil_ForInValidDateString() {
        sut = "2018-07-12T15:13:33"
        guard let date = Formatter.iso8601.date(from: sut) else  {
            return
        }
       XCTAssertNil(date)
    }
    
    func test_MonthFormatter_ReturnsCorrectAbbr_ForValidDateString() {
        sut = "2018-07-12T15:13:33.931Z"
        guard let date = Formatter.iso8601.date(from: sut) else {
            XCTFail("Must return correct abbreviation with correct date")
            return
        }
        XCTAssert(Formatter.monthAbbreviation.string(from: date) == "Jul")
    }
    
    func test_MonthFormatter_ReturnsNil_ForInValidDateString() {
        sut = "2018-07-12T15:13:33"
        guard let date = Formatter.iso8601.date(from: sut) else {
            return
        }
        let monthMedium = Formatter.monthAbbreviation.string(from: date)
        XCTAssertNil(monthMedium)
    }
}

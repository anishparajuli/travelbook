//
//  StubFetchTravelFeedsService.swift
//  TravelBookTests
//
//  Created by Anish on 7/31/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation
@testable import TravelBook

enum MockError: Error {
    case someError
}

extension MockError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .someError :
        return NSLocalizedString("Something happened unexpectedly", comment: "some error")
        }
    }
}

class StubFetchTravelFeedsService: TravelFeedsFetchable {
    var shouldFetchSuccess: Bool = false
    
    func fetchAllTravelLists(reqModel: NewsFeedRequestable, completion: @escaping ([NewsFeedModel]?, Error?) -> Void) {
        if shouldFetchSuccess {
            let newsFeedModel =
                [NewsFeedModel(userName: "bill", userProfileImageUrl: nil, slug: "Climb to Annapurna", coverImageUrl: nil, publishedDate: nil, isFavorite: true, coverImagesCount: nil),
                 NewsFeedModel(userName: "tom", userProfileImageUrl: nil, slug: "Party at Australia", coverImageUrl: nil, publishedDate: nil, isFavorite: true, coverImagesCount: nil),
                 NewsFeedModel(userName: "Hary", userProfileImageUrl: nil, slug: "Navigate to Office", coverImageUrl: nil, publishedDate: nil, isFavorite: true, coverImagesCount: nil),
                 NewsFeedModel(userName: "Gary", userProfileImageUrl: nil, slug: "Travel to Bali", coverImageUrl: nil, publishedDate: nil, isFavorite: true, coverImagesCount: nil),
                 NewsFeedModel(userName: "Trozan", userProfileImageUrl: nil, slug: "Climb to Annapurna", coverImageUrl: nil, publishedDate: nil, isFavorite: true, coverImagesCount: nil)
                ]
            completion(newsFeedModel, nil)
        }else {
            completion(nil, MockError.someError)
        }
    }
    
    func fetchSuccess() {
        shouldFetchSuccess = true
    }
    
    func fetchFailure() {
        shouldFetchSuccess = false
    }
}

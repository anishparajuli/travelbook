//
//  TravelCellViewModelTests.swift
//  TravelBookTests
//
//  Created by Anish on 7/30/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//



import XCTest
@testable import TravelBook

class TravelCellViewModelTests: XCTestCase {
    var sut: TravelFeedCellViewModel!

    override func tearDown() {
        sut = nil
    }
    
    func test_Initializer_SetsUserName_OfNewsFeedModel() {
       let model = NewsFeedModel()
       model.userName = "Bill John"
       sut = TravelFeedCellViewModel(model: model)
       XCTAssert(sut.userName == model.userName)
    }
    
    func test_Initializer_SetsSlug_OfNewsFeedModel() {
        let model = NewsFeedModel()
        model.slug = "Party at Thailand"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssert(sut.slug == model.slug)
    }
    
    func test_Initializer_SetsCorrectFormattedString_ForValidDate() {
        let model = NewsFeedModel()
        model.publishedDate = "2018-07-12T15:13:33.931Z"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssert(sut.formattedDateString == "JUL\n2018")
    }
    
    func test_Initializer_ReturnsNil_ForInValidDate() {
        let model = NewsFeedModel()
        model.publishedDate = "2018-07-12T15:"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertNil(sut.formattedDateString)
    }
    
    func test_Initializer_SetsValidPlaceImageURL_FromNewsfeedModel() {
        let model = NewsFeedModel()
        model.publishedDate = "https: // www."
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertNil(sut.placeImgUrl)
    }
    
    func test_Initializer_SetsImageURLNil_IfInCorrectString() {
        let model = NewsFeedModel()
        model.coverImageUrl = "https://www.youtube.com/"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertNotNil(sut.placeImgUrl)
    }
    
    func test_Initializer_SetsValidUserImageURL_FromNewsfeedModel() {
        let model = NewsFeedModel()
        model.userProfileImageUrl = "https://www.bill.com/"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertNotNil(sut.userImgUrl)
    }
    
    func test_Initializer_SetsUserProfileImageURLNil_ForInCorrectString() {
        let model = NewsFeedModel()
        model.userProfileImageUrl = "bill&com"
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertNil(sut.placeImgUrl)
    }
    
    func test_Variable_ShouldHideMediaView_ReturnsTrue_WhenMediaCountIsZero() {
        let model = NewsFeedModel()
        model.coverImagesCount = 0
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertTrue(sut.shouldHideMediaView)
    }
    
    func test_Variable_ShouldHideMediaView_ReturnsFalse_WhenMediaCountIsGreaterThanZero() {
        let model = NewsFeedModel()
        model.coverImagesCount = 1
        sut = TravelFeedCellViewModel(model: model)
        XCTAssertFalse(sut.shouldHideMediaView)
    }
}

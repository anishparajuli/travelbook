//
//  TravelFeedViewModel.swift
//  TravelBookTests
//
//  Created by Anish on 7/30/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import XCTest
@testable import TravelBook

class TravelFeedViewModelTests: XCTestCase {
    var sut: TravelFeedViewModel!

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }
    
    func test_func_fetchTravelFeeds_TriggersOnFeedsFetchSuccess_OnSuccess() {
        let expectation = self.expectation(description: "Scaling")
        let fetchFeedsService: TravelFeedsFetchable = StubFetchTravelFeedsService()
        (fetchFeedsService as! StubFetchTravelFeedsService).fetchSuccess()
        sut = TravelFeedViewModel(fetchFeedsService: fetchFeedsService)
        sut.onFeedsFetchSuccess = { data in
            expectation.fulfill()
            XCTAssert(data.count == 5)
            XCTAssert(data.first?.userName == "bill")
        }
        sut.fetchTravelFeeds()
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_func_fetchTravelFeeds_TriggersOnFeedsFetchFailure_OnFailure() {
        let expectation = self.expectation(description: "Scaling")
        let fetchFeedsService: TravelFeedsFetchable = StubFetchTravelFeedsService()
        (fetchFeedsService as! StubFetchTravelFeedsService).fetchFailure()
        sut = TravelFeedViewModel(fetchFeedsService: fetchFeedsService)
        sut.onFeedsFetchFailure =  { err in
            expectation.fulfill()
            print(err.localizedDescription)
            XCTAssert(err.localizedDescription == "Something happened unexpectedly")
        }
        sut.fetchTravelFeeds()
        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

//
//  TravelFeedCellViewModel.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

final class TravelFeedCellViewModel {
    var userName: String?
    var slug: String?
    var userImgUrl: URL?
    var formattedDateString: String?
    var mediaCount: Int?
    var placeImgUrl: URL?
    var shouldHideMediaView: Bool = false
    
    init(model: NewsFeedModel) {
        userName = model.userName
        slug = model.slug
        userImgUrl = URL(string: model.userProfileImageUrl ?? "")
        if  let publishedDate = model.publishedDate, let formattedDate = Formatter.iso8601.date(from: publishedDate) {
            let monthMedium = Formatter.monthAbbreviation.string(from: formattedDate)
            let formattedString = "\(monthMedium.uppercased())\n\(formattedDate.year)"
            formattedDateString = formattedString
        }
        mediaCount = model.coverImagesCount
        shouldHideMediaView = (mediaCount == 0)
        placeImgUrl = URL(string: model.coverImageUrl ?? "")
    }
}

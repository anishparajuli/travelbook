//
//  TravelFeedViewModel.swift
//  TravelBook
//
//  Created by Anish on 7/27/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

final class TravelFeedViewModel {
    private var travelFeedsService: TravelFeedsFetchable!
    var onFeedsFetchSuccess: (([NewsFeedModel]) -> Void) = {_ in }
    var onFeedsFetchFailure: ((Error) -> Void) = {_ in}
    private (set) var results: [NewsFeedModel] = []
    var isSortViewVisible: Bool = false
    var reqModel = NewsFeedRequest()
    
    init(fetchFeedsService: TravelFeedsFetchable) {
        self.travelFeedsService = fetchFeedsService
    }
    
    func fetchTravelFeeds() {
        travelFeedsService.fetchAllTravelLists(reqModel: reqModel) { [weak self] (response, err) in
            guard let strongSelf = self else {return}
            if let err = err {
                print("error occured", err)
                strongSelf.onFeedsFetchFailure(err)
            }else if let response = response {
                strongSelf.results = response
                strongSelf.onFeedsFetchSuccess(response)
            }
        }
    }
}

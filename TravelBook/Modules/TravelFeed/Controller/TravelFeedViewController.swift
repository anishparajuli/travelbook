//
//  ViewController.swift
//  TravelBook
//
//  Created by Anish on 7/27/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//


import UIKit

final class TravelFeedViewController: UIViewController {
    @IBOutlet weak var sortByWrapperView: UIView!
    @IBOutlet weak var travelBooksWrapperView: UIView!
    @IBOutlet weak var travelBooksLbl: LargeLabel!
    @IBOutlet weak var sortBylbl: MediumLabel!
    @IBOutlet weak var friendsBtn: SortButton!
    @IBOutlet weak var communityBtn: SortButton!
    @IBOutlet weak var feedsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sortByHeightConstrait: NSLayoutConstraint!
    @IBOutlet weak var dropDownImageView: UIImageView!
    
    @IBAction func friendsBtnActn(_ sender: Any) {
        friendsBtn.isSelected = true
        communityBtn.isSelected = false
        activityIndicator.startAnimating()
        travelFeedViewModel.reqModel.filter = "friends"
        travelFeedViewModel.fetchTravelFeeds()
    }
    
    @IBAction func communityBtnActn(_ sender: Any) {
        friendsBtn.isSelected = false
        communityBtn.isSelected = true
        activityIndicator.startAnimating()
        travelFeedViewModel.reqModel.filter = "community"
        travelFeedViewModel.fetchTravelFeeds()
    }
    
    var travelFeedViewModel: TravelFeedViewModel! 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        travelFeedViewModel.reqModel.filter = "friends"
        travelFeedViewModel.fetchTravelFeeds()
        configureView()
        setObservers()
        feedsTableView.addRefreshControl(msg: "Refreshing Feeds", onPull: #selector(TravelFeedViewController.onPull))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        friendsBtn.isSelected = true
    }
    
    deinit {
        print("TravelFeedViewController deinitialized")
    }
    
    private func addTapGestureInTravelBooksWrapperView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TravelFeedViewController.onTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        travelBooksWrapperView.addGestureRecognizer(tapGesture)
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        if !self.travelFeedViewModel.isSortViewVisible {
            showSortingView(imageRotationAngle: -CGFloat.pi)
        }else {
            hideSortingView(imageRotationAngle: 0.0)
        }
        self.travelFeedViewModel.isSortViewVisible = !self.travelFeedViewModel.isSortViewVisible
    }
    
    private func showSortingView(withAnimation: Bool = true, imageRotationAngle: CGFloat) {
        animateSortView(constant: 128, withAnimation, imageRotationAngle)
    }
    
    private func hideSortingView(withAnimation: Bool = true, imageRotationAngle: CGFloat) {
        animateSortView(constant: 0, withAnimation, imageRotationAngle)
    }
    
    private func animateSortView(constant: CGFloat, _ withAnimation: Bool = true, _ imageRotationAngle: CGFloat) {
        guard withAnimation else {
            self.sortByHeightConstrait.constant = constant
            self.dropDownImageView.transform = CGAffineTransform(rotationAngle: imageRotationAngle)
            return
        }
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.dropDownImageView.transform = CGAffineTransform(rotationAngle: imageRotationAngle)
            self?.sortByHeightConstrait.constant = constant
            self?.view.layoutIfNeeded()
        }
    }
    
    private func configureView() {
        hideSortingView(withAnimation: false, imageRotationAngle: 0.0)
        configureTableView()
        configureActivityIndicator()
        addTapGestureInTravelBooksWrapperView()
    }
    
    private func configureTableView() {
        self.feedsTableView.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.feedsTableView.dataSource = self
        self.feedsTableView.delegate = self
        self.feedsTableView.backgroundColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
        self.feedsTableView.estimatedRowHeight = 320
        self.feedsTableView.rowHeight = UITableView.automaticDimension
    }
    
    private func configureActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.red
    }
    
    private func setObservers() {
        setSuccessObserver()
        setOnFailureObserver()
    }
    
    private func setSuccessObserver() {
        self.travelFeedViewModel.onFeedsFetchSuccess = {[weak self] results in
            guard let strongSelf = self else {return}
            if strongSelf.feedsTableView.isRefreshing() {strongSelf.feedsTableView.endRefreshing()}
            strongSelf.activityIndicator.stopAnimating()
            results.count == 0 ? strongSelf.feedsTableView.add(backgroundMsg: "Travel feeds for this filter are too shy to show up. Please try again later") : strongSelf.feedsTableView.removeBackgroundMessage()
            strongSelf.feedsTableView.reloadData()
        }
    }
    
    private func setOnFailureObserver() {
        self.travelFeedViewModel.onFeedsFetchFailure = {[weak self] err in
            print("Some error occured", err)
            self?.activityIndicator.stopAnimating()
            self?.showAlert(withTitle: "Alert", message: err.localizedDescription)
        }
    }
}

//MARK: UITableViewDataSource
extension TravelFeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelFeedViewModel.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedTableViewCell
        return cell
    }
}

//MARK: UITableViewDelegate
extension TravelFeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? FeedTableViewCell)?.cellVM = TravelFeedCellViewModel(model: travelFeedViewModel.results[indexPath.row])
    }
}

//MARK: PullToRefresh
extension TravelFeedViewController {
    @objc func onPull() {
        travelFeedViewModel.reqModel.loadFirstPage()
        travelFeedViewModel.fetchTravelFeeds()
    }
}

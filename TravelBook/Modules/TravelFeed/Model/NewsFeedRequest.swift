//
//  NewsFeedRequest.swift
//  TravelBook
//
//  Created by Anish on 7/30/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct NewsFeedRequest: NewsFeedRequestable {
    var filter: String?
    var page: Int? = 1
    
    init() {
    }
    
    mutating func loadFirstPage() {
        self.page = 1
    }
}

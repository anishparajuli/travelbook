//
//  CodableDestination.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableDestination: Destination, Decodable {
    var data: MetaData?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decode(CodableMetaData.self, forKey: .data)
    }
}

//
//  CodableFeed.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableFeed: Feed, Decodable {
    var id: String?
    var type: String?
    var attributes: Attribute?
    var relationships: RelationShip?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case attributes
        case relationships
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        type = try values.decode(String.self, forKey: .type)
        attributes = try values.decode(CodableAttribute.self, forKey: .attributes)
        relationships = try values.decode(CodableRelationShip.self, forKey: .relationships)
    }
}

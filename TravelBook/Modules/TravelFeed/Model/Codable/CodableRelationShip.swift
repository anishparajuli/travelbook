//
//  CodableRelationShip.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableRelationShip: RelationShip, Decodable {
    var destination: Destination?
    var user: User?
    
    private enum CodingKeys: String, CodingKey {
        case destination
        case user
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        destination = try values.decode(CodableDestination.self, forKey: .destination)
        user = try values.decode(CodableUser.self, forKey: .user)
    }
}

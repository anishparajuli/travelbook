//
//  CodableTravelFeed.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableTravelFeed: TravelFeed, Decodable {
    var data: Array<Feed>?
    var included: Array<Included>?
    
    private enum CodingKeys: String, CodingKey {
        case data
        case included
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decode(Array<CodableFeed>.self, forKey: .data)
        included = try values.decodeIfPresent(Array<CodableIncluded>.self, forKey: .included)
    }
}


//
//  CodableAttribute.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableAttribute: Attribute, Decodable {
    var publishedAt: String?
    var likeCount: Int?
    var commentCount: Int?
    var isNew: Bool?
    var coverImageUrl: String?
    var mediaCount: Int?
    var slug: String?
    
    private enum CodingKeys: String, CodingKey {
        case publishedAt = "published_at"
        case likeCount = "likes_count"
        case commentCount = "comments_count"
        case isNew = "is_new"
        case coverImageUrl = "cover_image_url"
        case mediaCount = "media_count"
        case slug
    }
}

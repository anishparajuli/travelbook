//
//  CodableNewsFeedRequest.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableNewsFeedRequest: NewsFeedRequestable, Encodable {
    var filter: String?
    var page: Int? = 1
    
    init(filter: String?, page: Int?) {
        self.filter = filter
        self.page = page
    }
    
    private enum CodingKeys: String, CodingKey {
        case filter = "filter[scope]"
        case page
    }
    
    mutating func loadFirstPage() {
        self.page = 1
    }
}

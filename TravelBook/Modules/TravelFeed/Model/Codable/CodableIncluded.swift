//
//  CodableIncluded.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct CodableIncluded: Included, Decodable {
    var id: String?
    var type: String?
    var attributes: IncludedAtrribute?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case attributes
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        type = try values.decode(String.self, forKey: .type)
        attributes = try values.decode(CodableIncludedAttribute.self, forKey: .attributes)
    }
}

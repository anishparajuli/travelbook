//
//  NewsFeedModel.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

final class NewsFeedModel {
    var userName: String?
    var userProfileImageUrl: String?
    var slug: String?
    var coverImageUrl: String?
    var publishedDate: String?
    var isFavorite: Bool?
    var coverImagesCount: Int?
    
    init() {
        
    }
    
    init(userName: String?, userProfileImageUrl: String?, slug: String?, coverImageUrl: String?, publishedDate: String?, isFavorite: Bool?, coverImagesCount: Int?) {
        self.userName = userName
        self.userProfileImageUrl = userProfileImageUrl
        self.slug = slug
        self.coverImageUrl = coverImageUrl
        self.publishedDate = publishedDate
        self.isFavorite = isFavorite
        self.coverImagesCount = coverImagesCount
    }
}

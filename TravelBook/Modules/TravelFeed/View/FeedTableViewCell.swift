//
//  FeedTableViewCell.swift
//  NimbleSurvey
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

final class FeedTableViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImgView: RoundedImageView!
    @IBOutlet weak var userNameLbl: MediumLabel!
    @IBOutlet weak var dateLbl: SmallestLabel!
    @IBOutlet weak var locationCoverImgView: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var slugView: UIView!
    @IBOutlet weak var slugLbl: SmallLabel!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var mediaCountLbl: SmallestLabel!
    @IBOutlet weak var mediaImgView: UIImageView!
    @IBOutlet weak var mediaCountView: UIView!
   
    var cellVM: TravelFeedCellViewModel! {
        didSet {
            reloadUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        slugView.setCorner(radius: 6.0)
    }
    
    override func layoutSubviews() {
        mediaCountLbl.textColor = UIColor.white
    }
    
    private func configureView() {
        configureWrapperView()
        mediaImgView.setImage(name: "icon-media")
        self.userProfileImgView.contentMode = .scaleAspectFit
        slugView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        configureFavoriteBtn()
    }
    
    func configureWrapperView() {
        wrapperView.setCorner(radius: 12.0)
        wrapperView.backgroundColor = UIColor.white
        wrapperView.setBorder(width: 1.0, color: UIColor(red:0.8, green:0.82, blue:0.84, alpha:0.15))
        wrapperView.addShadow(of: UIColor(red:0, green:0, blue:0, alpha:0.1), radius: 14.0, offset: .zero, opacity: 1.0)
    }
    
    private func configureFavoriteBtn() {
        let image = UIImage(named: "icon-like-white")?.withRenderingMode(.alwaysTemplate)
        favoriteBtn.setImage(image, for: [])
        favoriteBtn.tintColor = UIColor.black
    }
    
    private func reloadUI () {
        userProfileImgView.setImage(from: cellVM.userImgUrl, placeHolder: "placeholder-man")
        userNameLbl.text = cellVM.userName
        dateLbl.text = cellVM.formattedDateString
        locationCoverImgView.setImage(from: cellVM.placeImgUrl, placeHolder: "placeholder-place")
        mediaCountLbl.text =  "\(cellVM.mediaCount ?? 0)"
        mediaCountView.isHidden = cellVM.shouldHideMediaView
        slugLbl.text = cellVM.slug ?? "N/A"
    }
}

//
//  TravelFeedsService.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol TravelFeedsFetchable {
    func fetchAllTravelLists(reqModel: NewsFeedRequestable, completion: @escaping([NewsFeedModel]?, Error?) -> Void)
}


class TravelFeedsService: TravelFeedsFetchable {
    func fetchAllTravelLists(reqModel: NewsFeedRequestable, completion: @escaping ([NewsFeedModel]? , Error?) -> Void) {
        let codableReqModel = CodableNewsFeedRequest(filter: reqModel.filter, page: reqModel.page)
        WebAPIClient.shared.request(url: AppURL.feeds, method: .get, parameters: codableReqModel.toDictionary()) {[weak self](data, error) in
            guard let strongSelf = self else {return}
            if let err = error {
                completion(nil, err)
            }else if let data = data {
                guard let res = data.decodeTo(type: CodableTravelFeed.self) else {
                    print("Data cannot be decoded to CodableTravelFeed")
                    return
                }
                let mappedResponse = strongSelf.mapRootResponseToNewsFeedModel(rootResponse: res) 
                completion(mappedResponse, nil)
            }
        }
    }
    
    private func mapRootResponseToNewsFeedModel(rootResponse: CodableTravelFeed) -> [NewsFeedModel] {
        let result = rootResponse.data.map { (feed) -> [NewsFeedModel] in
            return feed.map({ (model) -> NewsFeedModel in
                let newsFeedModel = NewsFeedModel()
                newsFeedModel.coverImagesCount = model.attributes?.mediaCount
                newsFeedModel.slug = model.attributes?.slug
                newsFeedModel.coverImageUrl = model.attributes?.coverImageUrl
                newsFeedModel.publishedDate = model.attributes?.publishedAt
                let includedData = rootResponse.included?.filter({ (obj) -> Bool in
                    return (obj.id == model.relationships?.user?.data?.id) && (model.relationships?.user?.data?.type == obj.type)
                }).first
                newsFeedModel.userName = includedData?.attributes?.name
                newsFeedModel.userProfileImageUrl = includedData?.attributes?.avatar
                return newsFeedModel
            })
        }
        return result ?? []
    }
}

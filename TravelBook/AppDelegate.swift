//
//  AppDelegate.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setInitialViewController()
        return true
    }
    
    private func setInitialViewController() {
        let travelFeedVC = ViewControllerRepository.getTravelFeedViewController()
        travelFeedVC.travelFeedViewModel = TravelFeedViewModel(fetchFeedsService: TravelFeedsService())
        window?.rootViewController = travelFeedVC
        window?.makeKeyAndVisible()
    }
}


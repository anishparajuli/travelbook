//
//  UIImageView.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//


import SDWebImage

extension UIImageView {
    func setImage(from url: URL?, placeHolder: String) {
        self.sd_setImage(with: url, placeholderImage: UIImage(named: placeHolder), completed: { image, error, _, _ in
            self.contentMode = .scaleAspectFill
        })
    }
    
    func setImage(name: String, tintColor color: UIColor = UIColor.white, withRenderingMode mode: UIImage.RenderingMode = .alwaysTemplate) {
        let image = UIImage(named: name)?.withRenderingMode(mode)
        self.image = image
        self.tintColor = color
    }
}

//
//  Date.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

extension Date {
    var year: String {
        let year = Calendar.current.component(.year, from: self)
        return "\(year)"
    }
}

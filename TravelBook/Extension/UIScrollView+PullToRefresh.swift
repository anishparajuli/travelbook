//
//  File.swift
//  TravelBook
//
//  Created by Anish on 7/30/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

private var handle: UInt8 = 0

extension UIScrollView {
    private var refreshcontrol: UIRefreshControl?{
        get {
            return objc_getAssociatedObject(self, &handle) as? UIRefreshControl? ?? nil
        } set {
            objc_setAssociatedObject(self, &handle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func addRefreshControl(msg: String, onPull: Selector) {
        let refreshControl: UIRefreshControl = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        refreshControl.tintColor = UIColor.black
        let textColor = [NSAttributedString.Key.foregroundColor: UIColor.black]
        refreshControl.attributedTitle = NSAttributedString(string: msg, attributes: textColor)
        self.refreshControl = refreshControl
        refreshControl.addTarget(onPull, action: onPull, for: .valueChanged)
        self.addSubview(refreshControl)
    }
    
    func isRefreshing() -> Bool {
        return self.refreshControl?.isRefreshing ?? false
    }
    
    func endRefreshing() {
        self.refreshControl?.endRefreshing()
    }
}

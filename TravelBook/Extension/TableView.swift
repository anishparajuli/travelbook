//
//  TableView.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

//to add error message in background view
extension UITableView{
    func add(backgroundMsg msg: String, textColor color: UIColor = .black) {
        let wrapperView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        let msgLbl = getMsgLabel(text: msg, textColor: color)
        msgLbl.frame = CGRect(x: 10, y: wrapperView.bounds.height / 2 - 25, width: wrapperView.bounds.width - 20, height: 50)
        wrapperView.addSubview(msgLbl)
        self.backgroundView = wrapperView
    }
    
    private func getMsgLabel(text: String, textColor color: UIColor) -> UILabel {
        let msgLbl = SmallLabel(frame: .zero)
        msgLbl.textAlignment = .center
        msgLbl.numberOfLines = 0
        msgLbl.textColor = color
        msgLbl.text = text
        return msgLbl
    }

    func removeBackgroundMessage() {
        self.backgroundView = nil
    }
}

//
//  File.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

extension Data {
    func decodeTo<T: Decodable>(type: T.Type) -> T? {
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(T.self, from: self)
            return decoded
        }catch {
            print(error)
        }
        return nil
    }
    
    var asJsonString: String? {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: self, options: [])
            return "\(String(describing: jsonObject))"
        } catch {
            print("Cannot Serialize Json \(error.localizedDescription)")
        }
        return nil
    }
}


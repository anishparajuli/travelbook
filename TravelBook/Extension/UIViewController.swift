//
//  UIViewController.swift
//  TravelBook
//
//  Created by Anish on 7/30/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(withTitle title: String, message: String? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

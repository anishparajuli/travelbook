
//
//  UIView.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

extension UIView {
    func setCorner(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func setBorder(width: CGFloat, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
   
    func addShadow(of color: UIColor, radius: CGFloat, offset: CGSize, opacity: Float) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
    }
    
    func removeShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
    }
}

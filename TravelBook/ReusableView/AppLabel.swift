//
//  BoldLabel.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLabel()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textColor = UIColor.white
        configureLabel()
    }
    
    func configureLabel() {
        self.font = UIFont(name: FontName.PlutoSansCond.Regular, size: 13)
    }
}

final class LargeLabel: BaseLabel {
    override func configureLabel() {
        self.textColor = UIColor(red: 56/255, green: 62/255, blue: 57/255, alpha: 1.0)
        self.textAlignment = .left
        self.font = UIFont(name: FontName.PlutoSansCond.Bold, size: 22)
    }
}

final class MediumLabel: BaseLabel {
    override func configureLabel() {
        self.font = UIFont(name: FontName.PlutoSansCond.Medium, size: 14)
        self.textAlignment = .left
        self.textColor = UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 1.0)
    }
}

final class SmallLabel: BaseLabel {
    override func configureLabel() {
        self.textColor = UIColor.white
        self.textAlignment = .left
        self.font = UIFont(name: FontName.PlutoSansCond.Medium, size: 12)
    }
}

final class SmallestLabel: BaseLabel {
    override func configureLabel() {
        self.font = UIFont(name: FontName.PlutoSansCond.Medium, size: 10)
        self.textAlignment = .center
        self.textColor = UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 1.0)
    }
}


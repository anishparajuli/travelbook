//
//  Button.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

class SortButton: UIButton {
    override var isSelected: Bool {
        didSet {
            self.backgroundColor = UIColor.white
            self.setCorner(radius: 14)
            let color =  isSelected ? UIColor(red: 236/255, green: 81/255, blue: 105/255, alpha: 1.0) : UIColor.black
            self.setTitleColor(color, for: .selected)
            let borderWidth: CGFloat = isSelected ? 1.0 : 0.0
            self.setBorder(width: borderWidth, color: UIColor(red: 0.8, green: 0.82, blue: 0.84, alpha: 0.15))
            if self.isSelected {
                 self.addShadow(of: UIColor(red: 0, green: 0, blue: 0, alpha: 0.1), radius: 14.0, offset: .zero, opacity: 1.0)
            }else {
                self.removeShadow()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name: FontName.PlutoSansCond.Medium, size: 14)
    }
}

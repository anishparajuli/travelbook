//
//  MetaData.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol MetaData {
    var id: String? {get set}
    var type: String? {get set}
}






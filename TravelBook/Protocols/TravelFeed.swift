//
//  TravelFeed.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol TravelFeed {
    var data: Array<Feed>? {get set}
    var included: Array<Included>? {get set}
}

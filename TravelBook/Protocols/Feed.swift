//
//  Feed.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol Feed: MetaData {
    var attributes: Attribute? {get set}
    var relationships: RelationShip? {get set}
}

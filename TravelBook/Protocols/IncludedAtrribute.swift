//
//  IncludedAtrribute.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol IncludedAtrribute {
    var avatar: String? {get set}
    var name: String? {get set}
}

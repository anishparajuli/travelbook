//
//  Attribute.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol Attribute {
    var publishedAt: String?  {get set}
    var likeCount: Int? {get set}
    var commentCount: Int? {get set}
    var isNew: Bool? {get set}
    var coverImageUrl: String? {get set}
    var mediaCount: Int? {get set}
    var slug: String? {get set}
}


//
//  RefreshTokenRespModel.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol RefreshTokenResponse {
    var accessToken: String? {get set}
    var tokenType: String?  {get set}
    var expiresIn: Int?  {get set}
    var refreshToken: String?  {get set}
    var createdAt: Int?  {get set}
}

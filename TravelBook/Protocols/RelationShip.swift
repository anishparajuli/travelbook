//
//  RelationShip.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol RelationShip {
    var destination: Destination? { get set }
    var user: User? {get set}
}

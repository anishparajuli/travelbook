//
//  2.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol Paginable {
    var page: Int? {get set}
    mutating func loadFirstPage()
}


//
//  RefreshTokenReqModel.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

protocol RefreshTokenRequest {
    var clientId: String {get set}
    var clientSecret: String {get set}
    var grantType: String {get set}
    var email: String {get set}
    var password: String {get set}
}

//
//  AppURL.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//


import Foundation

struct AppURL {
    private struct Routes {
        static let Api = "/api"
    }
    
    private struct Version {
        static let v1 = "/v1"
    }
    
    private  static let Domain: String  = "https://staging.travelbook.com"
    static let BuildingDomain = Domain
    static let Route = Routes.Api
    static let BaseURL = Domain + Route
    
    static var feeds: String {
        return BaseURL + Version.v1 + "/feeds"
    }
    
    static var refreshToken: String {
        return BaseURL + Version.v1 + "/token"
    }
}

//
//  Key.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct Key {
    static let AccessToken = "access_token"
}

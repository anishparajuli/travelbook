//
//  FontName.swift
//  TravelBook
//
//  Created by Anish on 7/29/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

struct FontName {
    struct PlutoSansCond {
        static let Regular = "PlutoSansCondRegular"
        static let Medium = "PlutoSansCondMedium"
        static let Bold = "PlutoSansCondBold"
    }
}

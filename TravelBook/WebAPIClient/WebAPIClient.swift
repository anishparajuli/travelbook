//
//  WebAPIClient.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Alamofire

final class WebAPIClient {
    private let validStatusCodes = 200...300
    private let requestTimeOutInterval: Double = 45.0
    private let responseTimeOutInterval: Double =  45.0
    static let shared = WebAPIClient(adapter: NetworkAdapter(), retrier: NetworkRetrier(refreshTokenService: RefreshTokenService()))
    private var alamofireSessionManger: Alamofire.SessionManager
    var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: Key.AccessToken)
        }set {
            UserDefaults.standard.set(newValue, forKey: Key.AccessToken)
        }
    }
    
    private init(adapter: NetworkAdapter, retrier: NetworkRetrier) {
        let urlConfiguration = URLSessionConfiguration.default
        urlConfiguration.timeoutIntervalForRequest = requestTimeOutInterval //secs
        urlConfiguration.timeoutIntervalForResource = requestTimeOutInterval//secs
        self.alamofireSessionManger = Alamofire.SessionManager(configuration: urlConfiguration)
        self.alamofireSessionManger.adapter = adapter
        self.alamofireSessionManger.retrier = retrier
    }
    
    func request(url: String, method: HTTPMethod, parameters: [String: Any]?, onCompletion: @escaping (Data?, Error?) -> Void) {
       alamofireSessionManger.request(url, method: method, parameters: parameters, encoding: method.encodingType, headers: nil).validate(statusCode: validStatusCodes).responseData { (response) in
        print("Request URL: ", response.request ?? "Cannot print request")
            switch response.result {
            case .failure(let error):
                print( String(data: response.data!, encoding: String.Encoding.utf8) ?? "Data could not be printed")
                onCompletion(nil, error)
            case .success(let data):
                print(data.asJsonString ?? "Cannot print Response Json")
                onCompletion(data, nil)
            }
        }
    }
}


//
//  RefreshTokenService.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//
import Alamofire

class RefreshTokenService {
    func refreshToken(parameters: [String: Any]?, completion: @escaping RefreshCompletion) {
        Alamofire.SessionManager.default.request(AppURL.refreshToken, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                print("Refresh Token URL:", response.request ?? "Cannot print request")
                print(response.data?.asJsonString ?? "Cannot print Response Json")
                switch(response.result) {
                case .success(  _):
                    print("Retry success")
                    let obj = try? JSONDecoder().decode(CodableRefreshTokenResponse.self,from: response.data!)
                    self.saveToUserDefaults(obj!)
                    completion(.success)
                case .failure(let err):
                    print("Session expired. Cannot refresh token", err)
                    completion(.failure(message: err.localizedDescription))
                }
        }
    }
    
    private func saveToUserDefaults(_ obj: RefreshTokenResponse) {
        WebAPIClient.shared.accessToken = obj.accessToken
    }
}

//
//  NetworkRetrier.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation
import Alamofire

typealias RefreshCompletion = (_ succeeded: RetryResult) -> Void

class NetworkRetrier: RequestRetrier {
    private let lock = NSLock()
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    private var refreshTokenService: RefreshTokenService!
    
    init(refreshTokenService: RefreshTokenService) {
        self.refreshTokenService = refreshTokenService
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 , request.retryCount < 1 {
            requestsToRetry.append(completion)
            if !isRefreshing {
                print("Token expired trying to refresh")
                refreshTokens { [weak self] succeeded in
                    guard let strongSelf = self else { return }
                    strongSelf.lock.lock()
                    defer { strongSelf.lock.unlock() }
                    switch succeeded {
                    case .failure(let message):
                        completion(false, 0.0)
                        strongSelf.showAlert(message)
                    case .success: print("Refresh Successs")
                    }
                    strongSelf.handleRetryAfterRefreshTokenService(succeeded: succeeded)
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        isRefreshing = true
        let reqModel = CodableRefreshTokenRequest()
        refreshTokenService.refreshToken(parameters: reqModel.toDictionary()) { res in
            completion (res)
        }
    }
    
    private func handleRetryAfterRefreshTokenService(succeeded: RetryResult) {
        self.requestsToRetry.forEach { $0(succeeded.retryResult, 0.0) }
        self.requestsToRetry.removeAll()
        self.isRefreshing = false
    }
    
    private func showAlert(_ message: String) {
        print("Retry error:", message)
    }
}

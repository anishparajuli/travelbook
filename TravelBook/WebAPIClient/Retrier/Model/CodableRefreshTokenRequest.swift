//
//  CodableRefreshTokenRequest.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

class CodableRefreshTokenRequest: RefreshTokenRequest, Encodable {
    var clientId: String = "3bb0640f3232379a9e07c0c44f9ef5e764eefb9ba0e1d31168a90ecebe2bc67d"
    var clientSecret: String = "073177b5f4f3489d46921c62629a42aa7b2bbdf57fc578bf2c61917957d037cc"
    var grantType: String = "password"
    var email: String = "olivier@nimbl3.com"
    var password: String = "12345678"
    
    private enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case grantType = "grant_type"
        case email
        case password
    }
}

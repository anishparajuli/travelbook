//
//  CodableRefreshToken.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

class CodableRefreshToken: RefreshToken, Decodable {
    var refreshToken: String?
    
    private enum CodingKeys: String, CodingKey {
        case refreshToken = "refresh_token"
    }
}

//
//  RetryResult.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation

enum RetryResult {
    case success
    case failure(message: String)
}

extension RetryResult {
    var retryResult: Bool {
        switch self {
        case .failure : return false
        default: return true
        }
    }
}

//
//  NetworkAdapter.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Foundation
import Alamofire

final class NetworkAdapter: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        guard let authToken = WebAPIClient.shared.accessToken else {
            return urlRequest
        }
        let params: Parameters = ["access_token": authToken]
        return try URLEncoding.default.encode(urlRequest, with: params)
    }
}



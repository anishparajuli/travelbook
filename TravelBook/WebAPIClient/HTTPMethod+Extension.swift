//
//  File.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import Alamofire

extension HTTPMethod{
    var encodingType:ParameterEncoding{
        switch self{
        case .get:
            return URLEncoding.default
        case .post:
            return JSONEncoding.default
        case .put:
            return JSONEncoding.default
        default:
            return JSONEncoding.default
        }
    }
}

//
//  ViewControllerRepository.swift
//  TravelBook
//
//  Created by Anish on 7/28/19.
//  Copyright © 2019 Anish Parajuli. All rights reserved.
//

import UIKit

struct ViewControllerRepository {
    static func getTravelFeedViewController() -> TravelFeedViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let travelFeedVC = storyboard.instantiateViewController(withIdentifier: "TravelFeedViewController") as! TravelFeedViewController
        return travelFeedVC
    }
}
